# Linear and Multiple Regression

This Python module provides functions to perform linear and multiple regression analysis on datasets. The main functionalities include:

- Computing the optimal slope for univariate regression.
- Computing the optimal slope and intercept for univariate regression.
- Choosing the best single predictor for multiple regression.
- Performing regression analysis using greedy residual fitting.
- Finding the optimal least squares regression using Gaussian elimination.

## Part I - Univariate Regression

### Task A: Optimal Slope

The `slope(x, y)` function computes the optimal least squares slope (a) for explaining y through x. It takes two lists of numbers (x and y) of equal length representing data of an explanatory and a target variable and returns the least squares slope.

```python
>>> slope([0, 1, 2], [0, 2, 4])
2.0
>>> slope([0, 2, 4], [0, 1, 2])
0.5
>>> slope([0, 1, 2], [1, 1, 2])
1.0
>>> slope([0, 1, 2], [1, 1.2, 2])
1.04

>>> X = [0, 1, 2]
>>> Y = [1, 1, 2]
>>> a = slope(X, Y)
>>> b = 0
>>> linear(a,b, x_min=0, x_max=2)
>>> plot2DData(X, Y)
>>> plt.show()
```
https://drive.google.com/file/d/1H_qK7yGbfub4OF7nIhtMSSeZDZ_c6g-f/view?usp=sharing

### Task B: Optimal Slope and Intercept

The `line(x, y)` function computes the least squares regression line (slope and intercept) for explaining y through x. It takes two lists of numbers (x and y) of equal length representing data of an explanatory and a target variable and returns the least squares slope (a) and intercept (b) as a tuple.

*Input*: Two lists of numbers (x and y) of equal length representing data of an explanatory and a target variable.

Output*: A tuple (a,b) where a is the optimal least squares slope and b the optimal intercept with respect to the given data.

```python
For example:
>>> line([0, 1, 2], [1, 1, 2])
(0.5, 0.8333333333333333)
```

### Task C: Choosing the Best Single Predictor

The `best_single_predictor(data, y)` function computes a general linear predictor by selecting the best univariate prediction model from all available explanatory variables. It takes table data with m rows and n columns representing data of n explanatory variables and a list y of length m containing the values of the target variable corresponding to the rows in the table. It returns a tuple (a, b) where a is a list of length n representing a linear predictor with weights, and b is a number representing the intercept.

*Input*: Table data with m > 0 rows and n > 0 columns representing data of n explanatory variables and a list y of length m containing the values of the target variable corresponding to the rows in the table.

*Output*: A pair (a, b) of a list a of length n and a number b representing a linear predictor with weights a and intercept b with the following properties:
a) There is only one non-zero element of a, i.e., there is one i in range(n) such that a[j]==0 for all indices j!=i.
b) The predictor represented by (a, b) has the smallest possible squared error among all predictors that satisfy property a).

```python
>>> data = [[1, 0],
... [2, 3],
... [4, 2]]
>>> y = [2, 1, 2]
>>> weights, b = best_single_predictor(data, y)
>>> weights, b
([0.0, -0.2857142857142858], 2.1428571428571432)
```

### Task D: Regression Analysis

Regression analysis can be performed using the functions mentioned above. Additional functionalities may include data cleaning, preprocessing, and visualization.

## Part II - Multivariate Regression

### Task A: Greedy Residual Fitting

The `greedy_predictor(data, y)` function implements a greedy correlation pursuit algorithm to perform multivariate regression. It takes a numpy data table data of explanatory variables with m rows and n columns and a list of corresponding target variables y and returns a tuple (a, b) where a is the weight vector and b is the intercept obtained by the greedy strategy.

*Input*: A data table data of explanatory variables with m rows and n columns and a list of corresponding target variables y.

*Output*: A tuple (a,b) where a is the weight vector and b is the intercept obtained by the greedy strategy.

```
For example (the first example is visualized in Figure 5)
>>> data = [[1, 0],
... [2, 3],
... [4, 2]]
>>> y = [2, 1, 2]
>>> weights, intercept = greedy_predictor(data, y)
>>> weights, intercept
[0.21428571428571436, -0.2857142857142858] 1.642857142857143
>>> data = [[0, 0],
... [1, 0],
... [0, -1]]
>>> y = [1, 0, 0]
>>> weights, intercept = greedy_predictor(data, y)
>>> weights, intercept
[-0.49999999999999994, 0.7499999999999999] 0.7499999999999999
```

### Task B: Optimal Least Squares Regression

This `equation(i, data, y)` function calculates the coefficients and the right-hand-side of a linear equation for an explanatory variable x(i) as specified in Equation (8) of specifications.

*Input*: Integer i with 0 <= i < n, data matrix data with m rows and n columns such that m > 0 and n > 0, and list of target values y of length n.

*Output*: Pair (c, d) where c is a list of coefficients of length n and d is a float representing the coefficients and right-hand-side of Equation 8 for data column i.

```python
For example (see Figure 6):
>>> data = [[1, 0],
... [2, 3],
... [4, 2]]
>>> target = [2, 1, 2]
>>> equation(0, data, target)
([4.666666666666666, 2.3333333333333326], 0.3333333333333326)
>>> equation(1, data, target)
([2.333333333333333, 4.666666666666666], -1.3333333333333335)
```

The `least_squares_predictor(data, y)` function finds the optimal least-squares predictor for the given data matrix and target vector. It takes a data matrix data with m rows and n columns and a list y of length m containing the values of the target variable corresponding to the rows in the table. It returns the optimal predictor (a, b) with weight vector a and intercept b such that a, b minimize the sum of squared residuals.

*Input*: Data matrix data with m rows and n columns such that m > 0 and n > 0.

*Output*: Optimal predictor (a, b) with weight vector a (len(a)==n) and intercept b such that a, b minimise the sum of squared residuals.

```python
>>> data = [[0, 0],
... [1, 0],
... [0, -1]]
>>> y = [1, 0, 0]
>>> weights, intercept = least_squares_predictor(data, y)
>>> weights, intercept
([-0.9999999999999997, 0.9999999999999997], 0.9999999999999998)
>>> data = [[1, 0],
... [2, 3],
... [4, 2]]
>>> y = [2, 1, 2]
>>> weights, intercept = least_squares_predictor(data, y)
>>> weights, intercept
([0.2857142857142855, -0.4285714285714285], 1.7142857142857149)
```

This module provides essential functionalities for performing linear and multiple regression analysis. For more details and examples, refer to the docstrings and example usage provided above.
